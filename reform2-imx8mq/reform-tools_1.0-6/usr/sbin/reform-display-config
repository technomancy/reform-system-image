#!/bin/bash

set -e

if [ "$EUID" -ne 0 ]
  then echo "reform-display-config has to be run as root / using sudo."
  exit
fi

DTB="x"
DTB_TARGET="imx8mq-mnt-reform2.dtb"

case "$1" in
	dual) DTB="imx8mq-mnt-reform2-dual-display.dtb";;
	single) DTB="imx8mq-mnt-reform2-single-display.dtb";;
esac

if [[ "$DTB" == "x" ]]
then
	echo "Usage: "
	echo "  reform-display-config dual      Select dual-display support (internal + HDMI)."
	echo "  reform-display-config single    Select only internal display (turns off HDMI)."
	exit
fi

MOUNTED_AT=$(mount | grep mmcblk1p1 | grep "on /" | cut -d ' ' -s -f 3)
if [[ "x$MOUNTED_AT" != "x" ]]
then
	if [[ -e "${MOUNTED_AT}/${DTB_TARGET}" ]] && [[ -e "${MOUNTED_AT}/${DTB}" ]]
	then
		echo "Found source and target files in $MOUNTED_AT."
		cp -v "${MOUNTED_AT}/${DTB}" "${MOUNTED_AT}/${DTB_TARGET}"
		echo "Restart MNT Reform (type: reboot) after saving your work to activate the changes."
		exit
	fi

	echo "Error: the SD card is already mounted at $MOUNTED_AT, but the DTB files ($DTB_TARGET and $DTB) are not there."
	exit
fi

echo "Assuming boot files are on SD card, but your system is not. Mounting /dev/mmcblk1p1 on /boot."

mount /dev/mmcblk1p1 /boot

if [[ -e "/boot/$DTB_TARGET" ]] && [[ -e "/boot/$DTB" ]]
then
	echo "Found source and target files in /boot."
	cp -v "/boot/$DTB" "/boot/$DTB_TARGET"
	echo "Restart MNT Reform (type: reboot) after saving your work to activate the changes."
	umount /boot
	exit
fi

echo "Error: could not find the DTB files ($DTB_TARGET and $DTB) in any of the expected places."

umount /boot
