# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi


# Defaults for MNT Reform

# enable NIR shader path in mesa. without this,
# some Xwayland applications will be blank
export ETNA_MESA_DEBUG=nir

# set GTK2 theme
export GTK2_RC_FILES=/usr/share/themes/Arc-Dark/gtk-2.0/gtkrc

unicode_start

if [ $(whoami) == "root" ]
then
   cat /etc/reform-root-help
elif [ ! -z $WAYLAND_DISPLAY ]
then
    # do nothing
    true
else
   cat /etc/reform-help
fi
