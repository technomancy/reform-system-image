#!/bin/bash
#
# This script installs extra applications that are not included in the minimal rescue image.
#

set -e

ETC=./template-etc
SKEL=./template-skel

# populate /etc
cp $ETC/motd target-userland/etc

chroot target-userland /bin/bash <<EOF
export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true
export LC_ALL=C
export LANGUAGE=C
export LANG=C

# install applications

apt install -y libreoffice libreoffice-gtk3 inkscape firefox-esr emacs gimp wmaker x11-utils
apt install -y chromium evolution freecad ardour sxiv neverball scummvm dosbox wf-recorder wev linphone-desktop

# install patched software from mntre

apt install -y kicad minetest libjsoncpp1 blender=2.79b+mntreform-2

# install a minimal gnome3

apt install -y --no-install-recommends gnome-control-center gnome-session

# remove apparmor (slows down boot)
apt remove -y apparmor

EOF
